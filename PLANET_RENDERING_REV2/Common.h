/**
* @file Common.h
* @brief Todo
*/

#pragma once

// STD include
#include <string>
#include <vector>
#include <map>

// SFML include
#define SFML_STATIC
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

// GLEW include
#define GLEW_STATIC
#include <GL/glew.h>
#include "glUtils.h"

// PHYSFS include
#include <physfs.h>

// Utils include
#include "Referenced.h"
#include "Clock.h"
#include "Math.h"

// Types include
#include "Types.h"