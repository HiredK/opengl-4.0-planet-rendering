/**
* @file smart_ptr.h
* @brief Smart pointer for handling referenced
* counted objects.
*/

#pragma once

template<class T>
class smart_ptr
{
	public:
		smart_ptr(const smart_ptr& rp);
		smart_ptr(T* t);
		smart_ptr();
		~smart_ptr();

		smart_ptr& operator = (const smart_ptr& rp);
		smart_ptr& operator = (T* ptr);

		bool operator == (const smart_ptr& rp) const;
		bool operator == (const T* ptr) const;

		bool operator != (const smart_ptr& rp) const;
		bool operator != (const T* ptr) const;

		bool operator < (const smart_ptr& rp) const;
		bool operator < (const T* ptr) const;

		const T& operator*() const;
		T& operator*();

		const T* operator->() const;
		T* operator->();

		bool operator!() const;

		bool valid() const;

		const T* get() const;
		T* get();

	private:
		T* m_ptr;
};

////////////////////////////////////////////////////////////////////////////////
// smart_ptr inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
template <class T>
inline smart_ptr<T>::smart_ptr(const smart_ptr& rp) : m_ptr(rp.m_ptr) {
	if (m_ptr) m_ptr->ref();
}
/*----------------------------------------------------------------------------*/
template <class T>
inline smart_ptr<T>::smart_ptr(T* t) : m_ptr(t) {
	if (m_ptr) m_ptr->ref();
}
/*----------------------------------------------------------------------------*/
template <class T>
inline smart_ptr<T>::smart_ptr() : m_ptr(nullptr) {
}
/*----------------------------------------------------------------------------*/
template <class T>
inline smart_ptr<T>::~smart_ptr() {
	if (m_ptr) m_ptr->unref();
}

/*----------------------------------------------------------------------------*/
template <class T>
inline smart_ptr<T>& smart_ptr<T>::operator = (const smart_ptr& rp) {
	if (m_ptr == rp.m_ptr) return *this;
	if (m_ptr) m_ptr->unref();
	m_ptr = rp.m_ptr;

	if (m_ptr) m_ptr->ref();
	return *this;
}
/*----------------------------------------------------------------------------*/
template <class T>
inline smart_ptr<T>& smart_ptr<T>::operator = (T* ptr) {
	if (m_ptr == ptr) return *this;
	if (m_ptr) m_ptr->unref();
	m_ptr = ptr;

	if (m_ptr) m_ptr->ref();
	return *this;
}

/*----------------------------------------------------------------------------*/
template <class T>
inline bool smart_ptr<T>::operator == (const smart_ptr& rp) const {
	return (m_ptr == rp.m_ptr);
}
/*----------------------------------------------------------------------------*/
template <class T>
inline bool smart_ptr<T>::operator == (const T* ptr) const {
	return (m_ptr == ptr);
}
/*----------------------------------------------------------------------------*/
template <class T>
inline bool smart_ptr<T>::operator != (const smart_ptr& rp) const {
	return (m_ptr != rp.m_ptr);
}
/*----------------------------------------------------------------------------*/
template <class T>
inline bool smart_ptr<T>::operator != (const T* ptr) const {
	return (m_ptr != ptr);
}
/*----------------------------------------------------------------------------*/
template <class T>
inline bool smart_ptr<T>::operator < (const smart_ptr& rp) const {
	return (m_ptr < rp.m_ptr);
}
/*----------------------------------------------------------------------------*/
template <class T>
inline bool smart_ptr<T>::operator < (const T* ptr) const {
	return (m_ptr < ptr);
}

/*----------------------------------------------------------------------------*/
template <class T>
inline const T& smart_ptr<T>::operator*() const {
	return *m_ptr;
}
/*----------------------------------------------------------------------------*/
template <class T>
inline T& smart_ptr<T>::operator*() {
	return *m_ptr;
}

/*----------------------------------------------------------------------------*/
template <class T>
inline const T* smart_ptr<T>::operator->() const {
	return m_ptr;
}
/*----------------------------------------------------------------------------*/
template <class T>
inline T* smart_ptr<T>::operator->() {
	return m_ptr;
}

/*----------------------------------------------------------------------------*/
template <class T>
inline bool smart_ptr<T>::operator!() const {
	return m_ptr == nullptr;
}
/*----------------------------------------------------------------------------*/
template <class T>
inline bool smart_ptr<T>::valid() const {
	return m_ptr != nullptr;
}

/*----------------------------------------------------------------------------*/
template <class T>
inline const T* smart_ptr<T>::get() const {
	return m_ptr;
}
/*----------------------------------------------------------------------------*/
template <class T>
inline T* smart_ptr<T>::get() {
	return m_ptr;
}