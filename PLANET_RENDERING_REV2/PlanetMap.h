/**
* @file PlanetMap.h
* @brief Generator for creating surface tiles
* for planets.
*/

#pragma once

#include "Program.h"

class PlanetMap : public Referenced
{
	public:
		// CTOR/DTOR:
		PlanetMap(const Planet& planet, GLsizei resolution);
		virtual ~PlanetMap();

		// SERVICES:
		void PrepareHeightMap(int face);
		void Bind(Program* prog) const;

	private:
		// MEMBERS:
		const Planet& m_planet;
		GLsizei m_resolution;
		GLuint m_heightmap;
		GLuint m_normalmap;
};

////////////////////////////////////////////////////////////////////////////////
// PlanetMap inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
inline void PlanetMap::Bind(Program* prog) const {
	if (m_heightmap) {
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_normalmap);
		prog->Uniform("s_NormalMap", 1);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_heightmap);
		prog->Uniform("s_HeightMap", 0);
	}
}

/*----------------------------------------------------------------------------*/
inline PlanetMap:: PlanetMap(const Planet& planet, GLsizei resolution):
m_planet(planet), m_resolution(resolution),
m_heightmap(GL_ZERO), m_normalmap(GL_ZERO){
}
/*----------------------------------------------------------------------------*/
inline PlanetMap::~PlanetMap() {
	if (m_heightmap) {
		glDeleteTextures(1, &m_heightmap);
		m_heightmap = GL_ZERO;
	}
	if (m_normalmap) {
		glDeleteTextures(1, &m_normalmap);
		m_normalmap = GL_ZERO;
	}
}