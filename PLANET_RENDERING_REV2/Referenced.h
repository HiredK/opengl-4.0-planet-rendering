/**
* @file Referenced.h
* @brief Base class from providing referencing
* counted objects.
*/

#pragma once

#include "smart_ptr.h"

class Referenced
{
	public:
		Referenced(const Referenced&);
		Referenced();

		// increment the reference count by one,
		// indicating that this object has another
		// pointer which is referencing it.
		void ref() const;

		// decrement the reference count by one,
		// indicating that a pointer to this object
		// is referencing it. If the refence count
		// goes to zero, it is assumed that this
		// object is no longer referenced and
		// is automatically deleted.
		void unref() const;

		// return the number pointers currently
		// referencing this object.
		int rcount() const;

	protected:
		virtual ~Referenced();
		mutable int m_rcount;
};

////////////////////////////////////////////////////////////////////////////////
// Referenced inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
inline void Referenced::ref() const {
	++m_rcount;
}
/*----------------------------------------------------------------------------*/
inline void Referenced::unref() const {
	if ((--m_rcount) <= 0) {
		delete this;
	}
}
/*----------------------------------------------------------------------------*/
inline int Referenced::rcount() const {
}

/*----------------------------------------------------------------------------*/
inline Referenced:: Referenced(const Referenced&) {
	m_rcount = 0;
}
/*----------------------------------------------------------------------------*/
inline Referenced:: Referenced() {
	m_rcount = 0;
}
/*----------------------------------------------------------------------------*/
inline Referenced::~Referenced() {
}