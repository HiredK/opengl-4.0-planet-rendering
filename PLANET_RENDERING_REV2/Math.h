/**
* @file Math.h
* @brief Includes the OpenGL Mathematics library.
* http://glm.g-truc.net/0.9.5/index.html
*/

#define GLM_FORCE_RADIANS
#undef e // to fix

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

typedef glm::ivec2 ivec2;
typedef glm::dvec2 dvec2;
typedef glm::vec2 vec2;

typedef glm::ivec3 ivec3;
typedef glm::dvec3 dvec3;
typedef glm::vec3 vec3;

typedef glm::ivec4 ivec4;
typedef glm::dvec4 dvec4;
typedef glm::vec4 vec4;

typedef glm::dquat dquat;
typedef glm::quat quat;

typedef glm::dmat3 dmat3;
typedef glm::mat3 mat3;

typedef glm::dmat4 dmat4;
typedef glm::mat4 mat4;

#pragma once