/**
* @file TessellatedGrid.h
* @brief Todo
*/

#pragma once

#include "Common.h"

class TessellatedGrid : public Referenced
{
	public:
		// CTOR/DTOR:
		TessellatedGrid(size_t size);
		virtual ~TessellatedGrid();

		// SERVICES:
		void Draw();

	private:
		// MEMBERS:
		GLuint m_VBuffer;
		GLuint m_EBuffer;
		size_t m_ENumber;
		size_t m_size;
};

////////////////////////////////////////////////////////////////////////////////
// Grid inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
inline TessellatedGrid::TessellatedGrid(size_t size) : m_size(size),
m_VBuffer(GL_ZERO), m_EBuffer(GL_ZERO), m_ENumber(0) {
}
/*----------------------------------------------------------------------------*/
inline TessellatedGrid::~TessellatedGrid() {
	if (m_VBuffer) {
		glDeleteBuffers(1, &m_VBuffer);
		m_VBuffer = GL_ZERO;
	}
	if (m_EBuffer) {
		glDeleteBuffers(1, &m_EBuffer);
		m_EBuffer = GL_ZERO;
		m_ENumber = 0;
	}
}