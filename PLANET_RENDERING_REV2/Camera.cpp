#include "Camera.h"

void Camera::Transform(float dt)
{
	// clamp velocity
	float velocitySpeed = glm::clamp(glm::length(m_velocity), 0.0f, m_maxVelocitySpeed);
	if (velocitySpeed > 0.0f) {
		m_velocity = glm::normalize(m_velocity) * velocitySpeed;
	}

	// clamp rotation
	float rotationSpeed = glm::clamp(glm::length(m_euler), 0.0f, m_maxRotationSpeed);
	if (rotationSpeed > 0.0f) {
		m_euler = glm::normalize(m_euler) * rotationSpeed;
	}

	// apply velocities
	m_position += m_velocity * dt;
	quat rotx = glm::angleAxis(glm::degrees(m_euler.x * dt), vec3(m_local[0]));
	quat roty = glm::angleAxis(glm::degrees(m_euler.y * dt), vec3(0, 1, 0));
	quat rotz = glm::angleAxis(glm::degrees(m_euler.z * dt), vec3(m_local[2]));
	m_rotation = glm::normalize(roty*rotx*rotz * m_rotation);

	// apply banking
	glm::quat bank = glm::angleAxis(glm::degrees(-m_euler.y * dt), glm::vec3(m_local[2]));

	// damp velocities
	m_velocity *= glm::pow(m_velocityDamp, dt);
	m_euler *= glm::pow(m_rotationDamp, dt);

	// build matrices
	m_current.pMatrix = glm::perspective(m_fov, m_aspect, m_near, m_far);
	m_current.vMatrix = glm::inverse(glm::translate(mat4(1), m_position) * glm::mat4_cast(bank * m_rotation));
	m_local = glm::translate(mat4(1), m_position) * glm::mat4_cast(m_rotation);
}