#include "Planet.h"

void Planet::Draw(Camera* cam, float dt)
{
	static smart_ptr<Program> program;
	if (!program.valid()) {
		program = new Program("data/planet.glsl");
		program->Bind();
	}

	// handle direct keyboard input
	static float LODFactor = 1.5f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		LODFactor += (LODFactor < 8.0f) ? 1.5f * dt : 0;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		LODFactor -= (LODFactor > 0.1f) ? 1.5f * dt : 0;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	const mat4 mvp = cam->GetProjMatrix() * cam->GetViewMatrix();
	program->UniformMat4("u_ModelViewProj", mvp);

	const vec3 pos = cam->GetPosition();
	program->Uniform("u_CamPosition", pos.x, pos.y, pos.z);
	program->Uniform("u_LODFactor", LODFactor);
	program->Uniform("u_Radius", m_radius);
	program->Uniform("u_Height", m_height);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	for (int i=0; i < 6; ++i) {
		if (!m_map[i].valid())
		{
			m_map[i] = new PlanetMap(*this, 2048);
			m_map[i]->PrepareHeightMap(i);
		}

		const mat3 trans = GetFaceTransform(i);
		program->Uniform("u_FaceTransform1", trans[0][0], trans[1][0], trans[2][0]);
		program->Uniform("u_FaceTransform2", trans[0][1], trans[1][1], trans[2][1]);
		program->Uniform("u_FaceTransform3", trans[0][2], trans[1][2], trans[2][2]);
		m_map[i]->Bind(program.get());
		m_grid->Draw();
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}