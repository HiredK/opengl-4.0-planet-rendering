/**
* @file Types.h
* @brief Forward declarations of class to avoid dependencies
*/

#pragma once

// Core
class Application;
class Camera;
class Clock;
class Program;

// Planet
class Planet;
class PlanetBrush;
class PlanetMap;
class TessellatedGrid;