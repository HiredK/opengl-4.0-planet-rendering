#include "PlanetMap.h"
#include "PlanetBrush.h"
#include "Planet.h"

void PlanetMap::PrepareHeightMap(int face)
{
	// create framebuffer object
	GLuint fbo = GL_ZERO;
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glVerifyState();

	// set the list of draw buffers
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers);
	glVerifyState();

	if (!m_heightmap) {
		static smart_ptr<Program> program;
		if (!program.valid()) {
			program = new Program("data/height.glsl");
			program->Bind();
		}

		std::vector<smart_ptr<PlanetBrush>> brushes;
		switch (face) {
			case Planet::RIGHT:
				brushes.push_back(new PlanetBrush("data/right.raw"));
				break;
			case Planet::LEFT:
				brushes.push_back(new PlanetBrush("data/left.raw"));
				break;
			case Planet::TOP:
				brushes.push_back(new PlanetBrush("data/top.raw"));
				break;
			case Planet::BOTTOM:
				brushes.push_back(new PlanetBrush("data/bottom.raw"));
				break;
			case Planet::FRONT:
				brushes.push_back(new PlanetBrush("data/front.raw"));
				break;
			case Planet::BACK:
				brushes.push_back(new PlanetBrush("data/back.raw"));
				break;

			default:
				break;
		}

		// create heightmap texture
		glGenTextures(1, &m_heightmap);
		glBindTexture(GL_TEXTURE_2D, m_heightmap);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R16, m_resolution, m_resolution, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		// set heightmap as attachement #0
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_heightmap, 0);
		glVerifyState();

		// enable rendering to heightmap
		glViewport(0, 0, m_resolution, m_resolution);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		Camera cam(1, atanf(1)*2, 1, float(m_resolution));
		cam.SetPosition(vec3(0, 0, m_resolution));

		program->UniformMat4("u_ModelViewProj", cam.GetProjMatrix() * cam.GetViewMatrix() *
		glm::scale(mat4(1), vec3(m_resolution, m_resolution, 1)));

		// blit all the created brushes
		for (size_t i=0; i < brushes.size(); i++) {
			brushes[i]->Blit(program.get());
		}

		// disable rendering to heightmap
		brushes.clear();
	}

	if (!m_normalmap) {
		static smart_ptr<Program> program;
		if (!program.valid()) {
			program = new Program("data/normal.glsl");
			program->Bind();
		}

		// create normalmap texture
		glGenTextures(1, &m_normalmap);
		glBindTexture(GL_TEXTURE_2D, m_normalmap);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, m_resolution, m_resolution, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		// set normalmap as attachement #0
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_normalmap, 0);
		glVerifyState();

		// enable rendering to normalmap
		glViewport(0, 0, m_resolution, m_resolution);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		program->Uniform("u_SampleDistance", 1.0f / (m_resolution + 1));
		program->Uniform("u_InverseSampleDistance", ((m_resolution + 1) << 0) * .25f);
		program->Uniform("u_HeightScale", 150.0f / 1500.0f);

		const mat3 trans = m_planet.GetFaceTransform(face);
		program->Uniform("u_FaceTransform1", trans[0][0], trans[1][0], trans[2][0]);
		program->Uniform("u_FaceTransform2",-trans[0][1],-trans[1][1],-trans[2][1]); // y invert here
		program->Uniform("u_FaceTransform3", trans[0][2], trans[1][2], trans[2][2]);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_heightmap);
		program->Uniform("s_HeightMap", 0);

		static float pos[] = {
			 1, 1, 0, 1,
			-1, 1, 0, 1,
			 1,-1, 0, 1,
			-1,-1, 0, 1
		};

		static float uv1[] = {
			 1, 1, 0, 1,
			 1, 0, 0, 0
		};

		static float uv2[] = {
			 1,-1,-1,-1,
			 1, 1,-1, 1
		};

		GLuint a_uv1 = program->GetAttribute("vs_TexCoord1");
		glEnableVertexAttribArray(a_uv1);
		glVertexAttribPointer(a_uv1, 2, GL_FLOAT, GL_FALSE, 0, uv1);
		glVerifyState();

		GLuint a_uv2 = program->GetAttribute("vs_TexCoord2");
		glEnableVertexAttribArray(a_uv2);
		glVertexAttribPointer(a_uv2, 2, GL_FLOAT, GL_FALSE, 0, uv2);
		glVerifyState();

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(4, GL_FLOAT, 0, pos);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glDisableClientState(GL_VERTEX_ARRAY);

		glDisableVertexAttribArray(a_uv2);
		glDisableVertexAttribArray(a_uv1);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteFramebuffers(1, &fbo);
	glDrawBuffer(GL_BACK);
	glVerifyState();
}