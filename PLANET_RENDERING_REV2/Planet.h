/**
* @file Planet.h
* @brief Entity representing a single planet object.
*/

#pragma once

#include "TessellatedGrid.h"
#include "PlanetMap.h"
#include "Camera.h"

class Planet : public Referenced
{
	public:
		// TYPEDEF/ENUMS:
		enum Face {
			RIGHT  = 0,
			LEFT   = 1,
			TOP    = 2,
			BOTTOM = 3,
			FRONT  = 4,
			BACK   = 5
		};

		// CTOR/DTOR:
		Planet(float radius, float height);
		virtual ~Planet();

		// SERVICES:
		mat3 GetFaceTransform(int i) const;
		void Draw(Camera* cam, float dt);

	private:
		smart_ptr<TessellatedGrid> m_grid;
		smart_ptr<PlanetMap> m_map[6];
		float m_radius;
		float m_height;
};

////////////////////////////////////////////////////////////////////////////////
// Planet inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
inline mat3 Planet::GetFaceTransform(int i) const {
	// Note, these are LHS transforms because the cube is rendered from
	// the inside, but seen from the outside. Hence there needs to be a
	// parity switch for each face.
	switch (i) {
		case RIGHT:
			return mat3(
				 0, 0, 1,
				 0, 1, 0,
				 1, 0, 0
			);
		case LEFT:
			return mat3(
				 0, 0,-1,
				 0, 1, 0,
				-1, 0, 0
			);
		case TOP:
			return mat3(
				 1, 0, 0,
				 0, 0, 1,
				 0, 1, 0
			);
		case BOTTOM:
			return mat3(
				 1, 0, 0,
				 0, 0,-1,
				 0,-1, 0
			);
		case FRONT:
			return mat3(
				-1, 0, 0,
				 0, 1, 0,
				 0, 0, 1
			);
		case BACK:
			return mat3(
				1, 0, 0,
				0, 1, 0,
				0, 0,-1
			);
		default:
			return mat3();
	}
}

/*----------------------------------------------------------------------------*/
inline Planet:: Planet(float radius, float height):
m_radius(radius), m_height(height),
m_grid(new TessellatedGrid(128)) {
}
/*----------------------------------------------------------------------------*/
inline Planet::~Planet() {
}